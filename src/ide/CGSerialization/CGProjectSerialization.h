/*
Cam�l�on Creator & Population 

Copyright 2012 O. Cugnon de S�vricourt & V. Tariel

This software is under the MIT license :
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the Software), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More informations can be found here: http://www.shinoe.org/cameleon
======================================*/
#ifndef CPROJECTSERIALIZATION_H
#define CPROJECTSERIALIZATION_H
#include <QtCore>
#include "CGProject.h"
#include "CGCompositionItem.h"
#include "CGComposition.h"
#include "CGCompositionManager.h"
#include "CGLayout.h"
#include "CGLayoutManager.h"
#include "CGLayoutFactory.h"
#include "CGProjectTree.h"
#include "CGChangeView.h"
#include "CClient.h"
#include "CGInstance.h"
class CGProjectSerialization : public QObject
{
    Q_OBJECT

signals:
    void progressSerial(int percent);
public:
    CGProjectSerialization();

    void load();
    void loadWithoutGui();

    static void saveAs(QString path, CGCompositionModel* topModel, QList<CGLayout*> layoutL, bool patterned=false, bool isInterface=false);
    static CGCompositionModel* compositionFromFile(QString file, CGCompositionModel* parentModel);
    static void createProject(QString path);
    static QString makeAbsolute(QString it);
    static QString makeRelative(QString it);
    static bool copyFile(QString oldFilePath, QString newFilePath);



};

#endif // CPROJECTSERIALIZATION_H
