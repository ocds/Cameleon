/*
Cam�l�on Creator & Population 

Copyright 2012 O. Cugnon de S�vricourt & V. Tariel

This software is under the MIT license :
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the Software), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

More informations can be found here: http://www.shinoe.org/cameleon
======================================*/
#ifndef CLAUNCHER_H
#define CLAUNCHER_H
#include <QtCore>
#include "CGProgressBar.h"
#include "CGComposer.h"
#include "CGController.h"
#include "CArguments.h"
class CLauncher : public QObject
{
    Q_OBJECT

public slots:
    void loadProgress(int percent);

    //MODE:
    /*
    IDE, integrated development environment (controller + composer)
    GUI, Graphical user interface (only controller)
    CLI command line interface
    */
    void start();
    void startGui();
    void startCli();

    void startIDE();
    void startWithoutProgress();

signals:
    void startLaunch();
    void update(int number, QString component, QString message);
public:
    CLauncher();
    void setDesktopSize(QSize size);
    void launch(QList<argument> appArgs);
    void launchCli(QList<argument> appArgs);
    void launchGui(QList<argument> appArgs);

private:
    void wait();
    QSize desktopSize;
    CGProgressBar * progress;
    CGComposer* composer;
    CGController* controller;
    bool first;


};

#endif // CLAUNCHER_H
