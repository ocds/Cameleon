SCDPATH = ../machine/scd
INCLUDEPATH += \
        $${SCDPATH} \
		$${SCDPATH}/CControl \
		$${SCDPATH}/COperator \
		$${SCDPATH}/CData \
		$${SCDPATH}/Common \
		$${SCDPATH}/COperator/Number \
		$${SCDPATH}/COperator/String \
		$${SCDPATH}/COperator/Boolean \
		$${SCDPATH}/COperator/Vector \
		$${SCDPATH}/COperator/Map \
		$${SCDPATH}/COperator/Table \
                $${SCDPATH}/COperator/DateTime \
		$${SCDPATH}/COperator/FileAndDirectory \
		$${SCDPATH}/COperator/Generic \
		$${SCDPATH}/COperator/FileAndDirectory \
		$${SCDPATH}/COperator/Map \
		$${SCDPATH}/COperator/Image \
		$${SCDPATH}/CControl/Boolean \
		$${SCDPATH}/CControl/Number\
		$${SCDPATH}/CControl/String \
		$${SCDPATH}/CControl/DateTime \
		$${SCDPATH}/CControl/FileAndDirectory \
		$${SCDPATH}/CControl/Image \
		$${SCDPATH}/CControl/Map \
		$${SCDPATH}/CControl/Table \
		$${SCDPATH}/CControl/Vector \
		$${SCDPATH}/CControl/Introspection \
                $${SCDPATH}/CControl/Interaction \
                $${SCDPATH}/COperator/Introspection/glossary \
                $${SCDPATH}/COperator/Introspection/project \
                $${SCDPATH}/COperator/Introspection/machineinstance \
                $${SCDPATH}/COperator/Control-Flow









