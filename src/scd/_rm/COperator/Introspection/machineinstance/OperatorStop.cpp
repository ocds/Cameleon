#include "OperatorStop.h"

#include "CLogger.h"
#include "CGCompositionManager.h"
OperatorStop::OperatorStop(){
    this->structurePlug().addPlugIn(DataGeneric::KEY,"input0.gen");
    this->structurePlug().addPlugOut(DataGeneric::KEY,"output0.gen");
    this->path().push_back("Introspection");
    this->path().push_back("Instance");
    this->setKey("OperatorStop");
    this->setName("Stop&Play");
    this->setInformation("pause & next the current Cam�l�on instance");
    this->setExecutedThread(COperator::CLIENT);
}

void OperatorStop::exec(){
    for(int i =0;i<(int)this->plugOut().size();i++)
        this->plugOut()[i]->getData()->dumpReception(this->plugIn()[i]->getData());

    CGCompositionManager::getInstance()->getComposer()->stop();
    CGCompositionManager::getInstance()->getComposer()->play();

}

COperator * OperatorStop::clone(){
    return new OperatorStop();
}
