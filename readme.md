<div align="center">
<p style="text-align: center;"><img src="deliverable/ressource/logo.png" align="middle" alt="Drawing" width="50"/></p>
</div>

What's this?
-------------------
Caméléon is a visual language.

Website: http://www.shinoe.org/cameleon

Licence: MIT LICENCE (see above)

Content
-------------------
* license.md: MIT license,
* src: the source code directory based on Qt 4.8,
* doc: the documentation in french (fr) and in english (en),
* blank: a Qt project to develop and build a Caméléon dictionary as a C++ shared library.


Licence
-------------------
This software is under the MIT license

Copyright (c) 2009-2018 O. Cugnon de Sévricourt & V. Tariel

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
