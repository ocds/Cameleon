Pré-requis
-------------------
Installation des dépendances:
``
apt-get install qt4-default
apt_get install libqtwebkit4
``

Installation
-------------------
* tar -xvf cameleon-deb-vx.y.zk.tar.gz
* cd cameleon-deb-vx.y.zk
* ./cameleon

Update your instance with a new Caméléon version
-------------------
* tar -xvf cameleon-deb-vx.y.zk.tar.gz
* copy content directory in your instance directory
