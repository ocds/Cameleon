<div align="center">
<p style="text-align: center;"><img src="img/logo.jpg" align="middle" alt="Drawing" width="100"/></p>
</div>

Documentation du langage Caméléon
=============

> Cette documentation est en construction et soumise à modification. Si vous souhaitez contribuer à son amélioration, n'hésitez pas à faire une proposition, soit:
> * en saisissant une demande de changement ici: [Demande de changement](issues)
> * en proposant un merge-request en suivant la procédure ici: [Guide du contributeur](./guide_contribution.md)

Présentation générale
-------------------
* [Introduction](./introduction.md)
* Présentation générale et argumentaire produit
* Site web /cameleon/fr

Documentation pour les utilisateurs
-------------------
* Installation raspbian [debian](./installation_debian.md) [windows](./installation_windows.md)
* Guide - Bonjour, oh monde ! (Hello world!) - Conception et construction
* Guide - Bonjour, oh monde ! (Hello world!) - Déploiement
* Guide - Bonjour, oh monde ! (Hello world!) - Patron
* Guide - Bases / guide rapide
* Guide - Partages de patrons et de compositions
* Guide - SCD / Les types standards
* Guide - SCD / Intéragir avec le système
* Guide - SCD / Génériques et flux de contrôle
* Guide - SCD / Boucles
* Guide - SCD / Intégration CLI
* Guide - SCD / Intégration API
* Guide - Caméléon lanceur en ligne de commande (lcm)
* Guide - Caméléon éditeur (ecm)
* [Grammaire](./grammaire.md)
* [Manuel de référence](./notice.md)

Documentation pour les contributeurs
-------------------
* [Guide de l'utilisateur contributeur](./guide_contribution.md)

Documentation pour les développeurs
-------------------
* Guide - Intégration d'un langage écrit
* Guide - Intégration C
* Guide - Intégration Java
* Guide - Intégration Python
* Guide - Intégration Php
* Guide - Intégration R
* Guide - Intégration Kotlin
* Guide - Intégration Go
* Guide - Intégration Perl
* Guide - Intégration Javascript
* Guide - Intégration Haskell
* Dev C++ Caméléon - Compilation raspbian [debian](./compilation_debian.md) [windows](./compilation_windows.md)
* Dev C++ Caméléon - Empaquetage raspbian [debian](./empaquetage_debian.md) [windows](./empaquetage_windows.md)
* Dev C++ Caméléon - Greffon C++
* [Spécification](./spec.md)

Usages
-------------------
* Usage - La brique en bois, un objet connecté
* Usage - CI/CD
* Usage - Automatisation de tests API
* Usage - [Traitement de photographies avec imagemagick](./usage_image.md)
* Usage - [Capatation et traitement de données avec python](./usage_twitter.md)
* Usage - Traitement d'image pour l'analyse scientifique de matériaux
* Usage - Entrainement de réseaux de neuronnes
* Usage - Réprésentation multi-échelle d'une organisation
* Usage - Conception papier

Pour aller plus loin
-------------------
* Safari#0 - Présentation générale
* Safari#1 - Etat de l'art et positionnement
* Safari#2 - L'histoire de Caméléon
* Safari#3 - Conception - BMG / SWOT
* Safari#4 - Conception - Kernel
* Safari#5 - Conception - Expérience utilisateur