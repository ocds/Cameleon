Notes de livraison
-------------------

* change deliverable/readme.txt with the good version x.y.z-testing or stable
* change deliverable/ressource/about.html with the good version x.y.z-testing or stable
* commit
* make a tag

* build & package

* change deliverable/readme.txt with the good version x.y.z+1-dev
* change deliverable/ressource/about.html with the good version x.y.z+1-dev
* commit -m "prepare "x.y.z+1-dev"
