Empaquetage
-------------------
* copy deliverable/* in a delivery zone (dir x.y.z for instance),
* copy qt*.dll in the bin directory of the delivery zone,
* create a log dir in the delivery zone
* zip the delivery zone as cameleon-x.y.z.zip