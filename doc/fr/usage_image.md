> Cette documentation est en construction et soumise à modification. 

Intégration
-------------------
L'histoire de l'intégration du script graytoning[1] basé sur ImageMagick écrit par Fred (http://www.fmwconcepts.com/imagemagick/).

Trois étapes pour une montée en échelle. Les patterns finaux sont disponibles en grapher[2].
* graytoning.pa
* graytoning-ctrl.pa
* graytoning-gui.pa

Composition 1
-------------------
L'histoire de la composition d'un traitement manuel de mélange de couche basé sur graytoning et permettant d'étalonner une image fixe noir et blanc.

Composition 2
-------------------
L'histoire de la composition d'un traitement automatique de mélange de couche basé sur graytoning et permettant de faire scintiller une vidéo.

Références
-------------------
* [1] http://www.fmwconcepts.com/imagemagick/graytoning/index.php
* [2] https://framagit.org/ocds/Grapher Grapher > Imagemagick > fred 