Pré-requis
-------------------
Installation des dépendances:
``
sudo apt-get install libqtcore4 libqtgui4
``

Installation
-------------------
* tar -xvf cameleon-deb-vx.y.zk.tar.gz

ou

* gzip -d cameleon-deb-vx.y.zk.tar.gz | tar -xf 

* cd cameleon-deb-vx.y.zk
* ./ecm

Mise à jour de votre version de travail
-------------------
* bck your files
* tar -xvf cameleon-deb-vx.y.zk.tar.gz
* copy content directory in your instance directory
* check that all is ok