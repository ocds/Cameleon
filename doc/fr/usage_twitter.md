> Cette documentation est en construction et soumise à modification. 

Intégration
-------------------
L'histoire de l'intégration du script écrit en python basé sur tweepy[1].

Trois patterns essentiels.
* search.pa: integration API twitter,
* rating.pa: pour évaluer la pertinence d'un tweet, écrit en python,
* generateHtmlFromTweetList.pa: pour faire un rapport d'analyse (paramètre sur la profondeur de la liste).

Composition
-------------------
L'histoire de la composition d'un traitement automatique en Caméléon et python afin de traiter de l'information en provenance d'une source externe exposant une API REST.

Références
-------------------
* [1] http://www.tweepy.org/