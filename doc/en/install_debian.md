Prerequisite
-------------------
Install dependencies:
``
apt-get install qt4-default
apt_get install libqtwebkit4
``

Install
-------------------
``
tar -xvf cameleon-deb-vx.y.zk.tar.gz
cd cameleon-deb-vx.y.zk
./cameleon
``

Update your working copy
-------------------
* tar -xvf cameleon-deb-vx.y.zk.tar.gz,
* copy content directory in your instance directory.
