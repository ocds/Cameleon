<div align="center">
<p style="text-align: center;"><img src="img/logo.jpg" align="middle" alt="Drawing" width="50"/></p>
</div>

Users documentation:
* Install [debian](./install_debian.md) [windows](./install_windows.md)
* [Concepts](./concepts.md)
* [Reference manual](./notice.md)

Developers documentation:
* Build [debian](./build_debian.md) [windows](./build_windows.md)
