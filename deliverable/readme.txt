##########################################
Cameleon v2.2.2-testing
MIT licence (see licence.txt)
Copyright Olivier Cugnon de Sevricourt et Vincent Tariel - 2009-2018
##########################################

1) Description
This directory contains Cameleon version 2.2.2-testing

2) Linux debian, ubuntu or raspbian launch
On Linux/Unix, you need to install qt4 libraries:
* apt-get install qt4-default
* ./ecm

3) Windows Launch
Double click on ecm.exe at the root directory