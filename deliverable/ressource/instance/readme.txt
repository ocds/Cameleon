##########################################
Cam�l�on & Population Community Edition

Cam�l�on Creator v2.0.3
Population library v3.2.2
Cam�l�on Machine v2.0.3

Copyright SHINOE SOFTWARE - 2012
##########################################

1) Description
This directory contain the Cam�l�on 2.0.3 beta 2 Community Edition & the Population 3.2.2 Community Edition.

2) Cam�l�on & Population
Cam�l�on & Population are the property of Shinoe Software Company (http://www.shinoe.org). 
Cam�l�on has been created by Olivier Cugnon de S�vricourt & Vincent Tariel. 
Population has been created by Vincent Tariel.
Cam�l�on has been invented by Olivier Cugnon de S�vricourt & Vincent Tariel (http://arxiv.org/abs/1110.4802)

3) Cam�l�on dev kit
The dev kit is a vailable using QT SDK & the generator project in project/devkit/generator (this is a Cam�l�on *.cm project file).

4) Source code
If you need to have access to the source code, please send-us your request describing your open source project from here: http://www.shinoe.org/cameleon/contact/.

Your open source project must meet the following criteria to be approved:
	- The project is licensed under a license approved by the Open Source Initiative.
	- The project source code is available for download.
	- Your open source project has a publicly accessible website.
	- Your Cam�l�on & Population work is accessible to the public. 

5) Third Party Dictionaries
The lib directory contain the first external Cam�l�on dictionary based on QCustomPlot (see here: http://www.workslikeclockwork.com). This dictionary is activated by default on the Community Editition.

6) Linux Launch
On Linux/Unix, you need to make the file executable in order to run it. You can either do this with your desktop�s file manager or, at the command line, type:
   chmod u+x cameleon.sh
You should now be able to execute the file as normal. You can do this from the command line by typing:
   ./cameleon-linux.sh.

7) Windows Launch
Double click on cameleon-windows.bat at the root directory